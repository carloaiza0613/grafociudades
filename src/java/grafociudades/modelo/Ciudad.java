/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafociudades.modelo;

import java.io.Serializable;

/**
 *
 * @author carloaiza
 */
public class Ciudad implements Serializable{
    private String nombre;
    private int nroHabitantes;
    private double areaMetros;
    private int posx;
    private int posy;

    public Ciudad() {
    }

    
    
    public Ciudad(String nombre, int nroHabitantes, double areaMetros) {
        this.nombre = nombre;
        this.nroHabitantes = nroHabitantes;
        this.areaMetros = areaMetros;
    }

    public Ciudad(String nombre, int nroHabitantes, double areaMetros, int posx, int posy) {
        this.nombre = nombre;
        this.nroHabitantes = nroHabitantes;
        this.areaMetros = areaMetros;
        this.posx = posx;
        this.posy = posy;
    }

    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNroHabitantes() {
        return nroHabitantes;
    }

    public void setNroHabitantes(int nroHabitantes) {
        this.nroHabitantes = nroHabitantes;
    }

    public double getAreaMetros() {
        return areaMetros;
    }

    public void setAreaMetros(double areaMetros) {
        this.areaMetros = areaMetros;
    }

    @Override
    public String toString() {
        return this.nombre;
    }

    public int getPosx() {
        return posx;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public int getPosy() {
        return posy;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }
    
    
    
    
}
